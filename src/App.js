import React from 'react';
import './App.css';
import { mDtaContext, gderContext, ctryContext } from './context';
import mock_data from "./data/MOCK_DATA.json";
import Cards from './components/cards-component';
import Country from './components/country-component';
import Gender from './components/gender-component';
import Header from './components/header-component';
import Search from './components/search-component';

function App() {
  let [mDtaState, setMDta] = React.useState(mock_data)
  let [gderState, setGder] = React.useState('')
  let [ctryState, setCtry] = React.useState('')

  const Layout = () => {
    return (
      <>
        <Header/>
        <Content/>
      </>
    )
  }

  const Content = () => {
    return (
      <>
        <Gender/><br/>
        <Country/><br/>
        <Search/>
        <Cards/>
      </>
    )
  }

  return (
    <mDtaContext.Provider value={[mDtaState, setMDta]}>
      <gderContext.Provider value={[gderState, setGder]}>
        <ctryContext.Provider value={[ctryState, setCtry]}>
          <Layout/>
        </ctryContext.Provider>
      </gderContext.Provider>
    </mDtaContext.Provider>
  )
}

export default App;
