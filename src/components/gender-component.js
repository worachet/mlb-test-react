import React from "react";
import { mDtaContext, gderContext, ctryContext } from '../context';
import mock_data from "../data/MOCK_DATA.json";
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

const BootstrapButton = styled(Button)({
  '&:hover': {
    borderColor: '#0062cc',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#0062cc',
    borderColor: '#005cbf',
  },
  '&:focus': {
    boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
  },
});

export default function Gender() {
    let [mDtaState, setMDta] = React.useContext(mDtaContext)
    let [gderState, setGder] = React.useContext(gderContext)
    let [ctryState, setCtry] = React.useContext(ctryContext)
    const buttons = React.useRef([])
    const genders = []

    const mockData = mock_data
    mockData.forEach(v => genders.indexOf(v.gender) === -1 ? genders.push(v.gender) : null)

    const onClickGender = (gender, index) => {
        let newData = mockData
        const names = ['country', 'gender']
        const conds = { 'country': ctryState, 'gender': gender }
        names.forEach(n => newData = newData.filter(v => conds[n] ? v[n] === conds[n] : true))
        setGder(gender)
        setMDta(newData)
    }

    React.useEffect(() => {
        
    }, [mDtaState])

    return (
        <Stack spacing={2} direction="row" justifyContent="center">
            {genders.map((v, i) => {
                return (
                    <BootstrapButton variant="outlined" disableRipple
                        key={i}
                        ref={el => buttons.current[i] = el}
                        onClick={() => onClickGender(v, i)}>{v}
                    </BootstrapButton>
                )
            })}
        </Stack>
    );
}