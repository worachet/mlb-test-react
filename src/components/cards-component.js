import React from "react";
import { mDtaContext } from "../context";
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    color: theme.palette.text.secondary,
    paddingTop: 30,
    textAlign: 'center',
  }));

export default function Cards() {
    let [mDtaState, setMDta] = React.useContext(mDtaContext)

    return (
        <Grid container spacing={5} >
            {mDtaState.map((v, i) => {
                return (
                    <Grid key={i} item xs={4}>
                      <Item elevation={0}>
                        <img src={v.image} alt="" width={400} height={200} />
                        <article>
                            <p>{v.first_name} {v.last_name}</p>
                            <p><b>{v.gender}</b></p>
                            <p><b>{v.email}</b></p>
                            <p><b>{v.country}</b></p>
                        </article>
                      </Item>
                    </Grid>
                )
            })}
        </Grid>
    );
}