import React from "react";
import { mDtaContext, gderContext, ctryContext } from '../context';
import mock_data from "../data/MOCK_DATA.json";
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

const BootstrapButton = styled(Button)({
  '&:hover': {
    borderColor: '#0062cc',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#0062cc',
    borderColor: '#005cbf',
  },
  '&:focus': {
    boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
  },
});

export default function Country() {
    let [mDtaState, setMDta] = React.useContext(mDtaContext)
    let [gderState, setGder] = React.useContext(gderContext)
    let [ctryState, setCtry] = React.useContext(ctryContext)
    const buttons = React.useRef([]) 
    const countries = []

    const mockData = mock_data
    mockData.forEach(v => countries.indexOf(v.country) === -1 ? countries.push(v.country) : null)

    const onClickCountry = (country, index) => {
        buttons.current[index].style.backgroundColor = "#FFFFF"
        let newData = mockData
        const names = ['country', 'gender']
        const conds = { 'country': country, 'gender': gderState }
        names.forEach(n => newData = newData.filter(v => conds[n] ? v[n] === conds[n] : true))
        setCtry(country)
        setMDta(newData)
    }

    React.useEffect(() => {
        
    }, [mDtaState])

    return (
        <Stack spacing={2} direction="row" justifyContent="center">
            {countries.map((v, i) => {
                return (
                    <BootstrapButton variant="outlined" disableRipple
                        key={i}
                        ref={el => buttons.current[i] = el}
                        onClick={() => onClickCountry(v, i)}>{v}
                    </BootstrapButton>
                )
            })}
        </Stack>
    );
}