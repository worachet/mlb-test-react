import React from "react";
import { mDtaContext } from '../context';
import mock_data from "../data/MOCK_DATA.json";
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';

const BootstrapButton = styled(Button)({
    '&:hover': {
      boxShadow: 'none',
      borderColor: '#0062cc',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
});

export default function Search() {
    let [mDtaState, setMDta] = React.useContext(mDtaContext)
    const input = React.useRef()
    const button = React.useRef()
    const mockData = mock_data

    const handleKeyUp = (ev) => {
        if (ev.key === 'Enter') {
            let newData = mockData
            const nameArr = ev.target.value.toLowerCase().split(' ')
            nameArr.forEach((v, i) => { if(!v) nameArr.splice(i, 1) })
            newData = newData.filter(v => {
                if (nameArr.length === 1) {
                    return v.first_name.toLowerCase().startsWith(nameArr[0]) 
                        || v.last_name.toLowerCase().startsWith(nameArr[0])
                } else {
                    return v.first_name.toLowerCase().startsWith(nameArr[0]) 
                        && v.last_name.toLowerCase().startsWith(nameArr[1])
                }
            })
            setMDta(newData)
        }
    }

    const onClickClear = () => {
        input.current.value = ""
        setMDta(mockData)
    }

    return (
        <Stack spacing={2} direction="row" justifyContent="center">
            <TextField 
                ref={input}
                id="outlined-basic" 
                label="Search" 
                variant="outlined" 
                type="text" 
                placeholder="Search" 
                size="small"
                onKeyUp={handleKeyUp}
                />
            <BootstrapButton disableRipple
                ref={button}
                onClick={() => onClickClear()}>CLEAR
            </BootstrapButton>
        </Stack>
    );
}